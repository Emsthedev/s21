console.log("Hello");
console.log("");


/*  */

let studentNumA= "2020-1923";
let studentNumB = "2020-1924";
let studentNumC= "2020-1925";
let studentNumD= "2020-1926";
let studentNumE= "2020-1927";

console.log("STUDENT A: " +studentNumA);
console.log("STUDENT B: " +studentNumB);
console.log("STUDENT C: " +studentNumC);
console.log("STUDENT D: " +studentNumD);
console.log("STUDENT E: " +studentNumE);


console.log("");
let studentID = ["2020-1923","2020-1924","2020-1925","2020-1926","2020-1927"]
console.log("StudentID : " +studentID);

/*  

        Arrays 



        Syntax:
        let/const arrayName = [elementA, elementB, .... elementN];




*/
console.log("");
let grades = [96.8, 85.7, 93.2, 94.6];
let computerBrands = ["Acer","Asus", "Lenovo", "Neo", "RedFox", "Toshiba", "Fujitsu"];
console.log("grades" + grades);
console.log("Comp Brands: "+computerBrands);

console.log("");
console.log("Mixed Arrays");
let MixArr = [12, "Asus", null, undefined, {}]; //not
console.log("Mixed: " + MixArr);

console.log("");
console.log("myTask");

let myTasks = [

    "drink html",
    "eat javascript",
    "inhale css" , 
    "bake sass"
];
console.log(myTasks);

console.log("");
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "New York";
let city4 = "Beijing";

let cities = [city1,city2,city3,city4];
let citiesSample = ["Tokyo","Manila","New York","Beijing"];
console.log(cities);
// console.log(typeof citiesSample);
// console.log(cities === citiesSample);

/* park ko lang dito sir, balikan nyo na lang po after ng discussion nyo
car1 ="honda";
car2 ="mitsubishi";
const array1 = [car1,car2];
const array2 = ["honda","mitsubishi"];

console.log(array1.trim == array2.trim);
may space somewhere, need nyo sya ih trim
nagtrue ang result */

//Array
/* Array Length Property
    -to set or to get the items or elements
        in an array.
        
        */
        console.log("");
        console.log(myTasks.length) //4
        console.log(cities.length) //4
        console.log("");
        let blankArr = [];
        console.log(blankArr.length) // 0
        console.log("");
        let fullName = "Fidel Ramos";
        console.log(fullName.length) //11 character including space

        console.log("");
        myTasks.length--
        console.log(myTasks.length)//3
        console.log(myTasks);

        
        console.log("");
        let theBeatles = ['John', 'Paul', 'Ringo','George'];
        console.log(theBeatles.length)
        theBeatles.length++
        console.log(theBeatles.length)
        console.log(theBeatles)

        theBeatles[4] = 'Yoby';
        console.log(theBeatles)

    /* Access Element of an Array 

    Syntax:
    arrrayName[indexNumber];

    */
        console.log("");

        console.log(grades[0]); //96.8
        console.log(computerBrands[3]) //Neo

        console.log("");
        let lakersLegend = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem']

console.log(lakersLegend[0]);
console.log(lakersLegend[2])


let currentLaker = lakersLegend[2];


console.log(currentLaker);

console.log("");
console.log("Array before the reassignment");
console.log(lakersLegend);
lakersLegend[2] = 'Pau Gasol';
console.log("Array after the reassignment");
console.log(lakersLegend);

console.log("");
let bullsLegends = ['Jordan' , 'Pipen' ,'Rodman' , 'Rose', 'Kukoc'];

let lastElementIndex = bullsLegends.length - 1;
console.log(bullsLegends.length)
console.log(bullsLegends[lastElementIndex]);


//Adding items into an array
console.log("");
let newArr = [];
console.log(newArr[0]);
newArr[0] = 'Jenny';
console.log(newArr);
newArr[1] = 'Jisoo';
console.log(newArr);

//from last will add items
console.log("");
newArr[newArr.length++] = 'Lisa';
console.log(newArr);


//Mini Activity 
console.log("");
let superHeroes = ['IronMan','The Hulk' ,'SpiderMan'];
console.log(superHeroes);
function miniActivityp(addHero){
   superHeroes[superHeroes.length++] = addHero;

}
miniActivityp('Thor');
console.log(superHeroes);

//miniactivty 2
console.log("");
let heroFound ;


function callHeroes(getHeroes){
   heroFound = superHeroes[getHeroes];
    
}

callHeroes(1);
console.log(heroFound);



console.log("");

for (let index = 0; index < newArr.length; index++) {
    console.log(newArr[index]);
    
}
console.log("");

let numArr = [5, 12 ,26,30, 42, 50, 67, 85];
for (let i = 0; i < numArr.length; i++) {
        if(numArr[i] % 5 === 0 ){
            console.log("is divisible by 5 : " + numArr[i]);
        }else {
            console.log("is not divisible by 5 : " + numArr[i])
        }
    

       
}

 /* Multi Dimensional Arrays 
        -arrays within an arrays */

        let chessBoard = [

            ['a1','b1','c1','d1','e1','f1','g1','h1'],
            ['a2','b2','c2','d2','e2','f2','g2','h2'],
            ['a3','b3','c3','d3','e3','f3','g3','h3'],
            ['a4','b4','c4','d4','e4','f4','g4','h4'],
            ['a5','b5','c5','d5','e5','f5','g5','h5'],
            ['a6','b6','c6','d6','e6','f6','g6','h6'],
            ['a7','b7','c7','d7','e7','f7','g7','h7'],
            ['a8','b8','c8','d8','e8','f8','g8','h8']
          


        ]

        console.log(chessBoard);

        console.log(chessBoard[1][4])
        console.log("pawn moves to : " + chessBoard[7][4]);
